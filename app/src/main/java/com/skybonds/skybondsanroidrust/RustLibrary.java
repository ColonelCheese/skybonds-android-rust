package com.skybonds.skybondsanroidrust;

public class RustLibrary {

    private static native String hello(final String pattern);

    public String helloworld(String string) {
        return hello(string);
    }
}
