package com.skybonds.skybondsanroidrust;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class RustActivity extends AppCompatActivity {

    static {
        System.loadLibrary("helloworld");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rust);

        RustLibrary g = new RustLibrary();
        String r = g.helloworld("world");

        ((TextView)findViewById(R.id.text)).setText(r);
    }
}
