#!/bin/sh

set -eu

cd cargo

cargo build --target aarch64-linux-android --release
cargo build --target armv7-linux-androideabi --release
cargo build --target i686-linux-android --release

echo 'Build libs...'

cd ../app/src/main

mkdir -p jniLibs
mkdir -p jniLibs/arm64
mkdir -p jniLibs/armeabi
mkdir -p jniLibs/x86

echo 'Mk jniLibs...'

cd ../../..

ln -s $PWD/cargo/target/aarch64-linux-android/release/libhelloworld.so $PWD/app/src/main/jniLibs/arm64/libhelloworld.so
ln -s $PWD/cargo/target/armv7-linux-androideabi/release/libhelloworld.so $PWD/app/src/main/jniLibs/armeabi/libhelloworld.so
ln -s $PWD/cargo/target/i686-linux-android/release/libhelloworld.so $PWD/app/src/main/jniLibs/x86/libhelloworld.so

echo 'Copy links...'
