#!/bin/sh

set -eu

if [ -d ndk ]; then
    printf '\033[33;1mStandalone ndk already exists... Delete the ndk folder to make a new one.\033[0m\n\n'
    printf '  $ rm -rf ndk\n'
    exit 0
fi

export NDK_HOME=$ANDROID_HOME/ndk-bundle

if [ ! -d "${NDK_HOME-}" ]; then
    NDK_HOME="$ANDROID_HOME/ndk-bundle"
fi

MAKER="${NDK_HOME}/build/tools/make_standalone_toolchain.py"

if [ -x "$MAKER" ]; then
    echo 'Creating standalone ndk...'
else
    printf '\033[91;1mPlease install Android ndk!\033[0m\n\n'
    printf '  $ sdkmanager ndk-bundle\n\n'
    printf "\033[33;1mnote\033[0m: file \033[34;4m$MAKER\033[0m not found.\n"
    printf 'If you have installed the NDK in non-standard location, please define the \033[1m$NDK_HOME\033[0m variable.\n'
    exit 1
fi

mkdir ndk

create_ndk() {
    echo "($1)..."
    "$MAKER" --api "$2" --arch "$1" --install-dir "ndk/$1"
}

create_ndk arm64 26
create_ndk arm 26
create_ndk x86 26

echo 'Updating cargo-config.toml...'

sed 's|$PWD|'"${PWD}"'|g' cargo-config.toml.template > ndk/cargo-config.toml
